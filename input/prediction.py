

# Importing the libraries
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

# Importing the dataset
dataset = pd.read_csv('train.csv')
test_dataset = pd.read_csv('titanic_test.csv')
#test_result = pd.read_csv('gender_submission.csv')
"""survival - Survival (0 = No; 1 = Yes)
class - Passenger Class (1 = 1st; 2 = 2nd; 3 = 3rd)
name - Name
sex - Sex
age - Age
sibsp - Number of Siblings/Spouses Aboard
parch - Number of Parents/Children Aboard
ticket - Ticket Number
fare - Passenger Fare
cabin - Cabin
embarked - Port of Embarkation (C = Cherbourg; Q = Queenstown; S = Southampton)
boat - Lifeboat (if survived)
body - Body number (if did not survive and body was recovered)"""
features = ['Pclass', 'Sex', 'Age', 'SibSp', 'Parch']
X = dataset.loc[:, features].values
y = dataset.loc[:, ['Survived']].values

x_test = test_dataset.loc[:, features].values
y_test = test_dataset.loc[:, ['Survived']].values

# Dealing with missing data
from sklearn.preprocessing import Imputer
imputer = Imputer(strategy = 'mean')
imputer = imputer.fit(X[:, 2:])
X[:, 2:] = imputer.transform(X[:, 2:])

test_imputer = Imputer(strategy = 'mean')
test_imputer = test_imputer.fit(x_test[:, 2:])
x_test[:, 2:] = test_imputer.transform(x_test[:, 2:])

# Encoding sex
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
label_enc = LabelEncoder()
X[:, 0] = label_enc.fit_transform(X[:, 0])
X[:, 1] = label_enc.fit_transform(X[:, 1])

x_test[:, 0] = label_enc.fit_transform(x_test[:, 0])
x_test[:, 1] = label_enc.fit_transform(x_test[:, 1])
hot_enc = OneHotEncoder(categorical_features = [0, 1])
X = hot_enc.fit_transform(X).toarray()
x_test = hot_enc.fit_transform(x_test).toarray()

# Feature Scaling
from sklearn.preprocessing import StandardScaler
sc = StandardScaler()
X = sc.fit_transform(X)

# Fitting Kernel SVM to the Training set
from sklearn.svm import SVC
classifier = SVC(kernel = 'rbf', random_state = 0)
classifier.fit(X, y)

y_pred = classifier.predict(X)

# Evaluation
from sklearn.metrics import confusion_matrix
cm = confusion_matrix(y, y_pred)

ar = (cm[0, 0] + cm[1, 1])/cm.sum()

y_test_pred = classifier.predict(x_test)
cm_test = confusion_matrix(y_test, y_test_pred)
test_ar = (cm_test[0, 0] + cm_test[1, 1])/cm_test.sum()
